# README #
Minimal terminal snake game.

![](snake.gif)

# Clone repo #
`$ git clone https://bitbucket.org/br3gan/snake.git`

# Build #
from repository directory, run:

`$ mkdir build && cd build && g++ ../src/*.cpp -o snake`

# Run #
from build directory, run:

`$ ./snake [dimension]`

# Instructions #
* Easy: Snake can pass through the walls.

* Hard: Snake crushes on the walls.

* Expert: Additional obstacles introduced.
